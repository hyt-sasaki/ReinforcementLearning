function drawMaze(maze, canvas) {
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    var mazeArray = maze.mazeArray;
    var V = mazeArray.length;
    var H = mazeArray[0].length;

    var cellWidth = canvas.width / H;
    var cellHeight = canvas.height / V;

    var d = [[0, 0], [cellWidth, 0], [cellWidth, cellHeight], [0, cellHeight]];
    for (var i=0; i < mazeArray.length; ++i) {
        for (var j=0; j < mazeArray[0].length; ++j) {
            if (mazeArray[i][j] === maze.PASSAGE || mazeArray[i][j] === maze.WALL) {
                var c = [];
                for (var k=0; k < 4; ++k) {
                    c[k] = [cellWidth * j + d[k][0], cellHeight * i + d[k][1]];
                }

                ctx.beginPath();
                ctx.moveTo(c[0][0], c[0][1]);
                for (var k=1; k < c.length; ++k) {
                    ctx.lineTo(c[k][0], c[k][1]);
                }
                ctx.closePath();
                if (mazeArray[i][j] === maze.PASSAGE) {
                    ctx.fillStyle = 'rgb(255, 255, 255)';
                } else if (mazeArray[i][j] === maze.WALL) {
                    ctx.fillStyle = 'rgb(0, 0, 0)';
                }
                ctx.fill();
                ctx.fillStyle = 'rgb(0, 0, 0)';
            } else {
                var fontSize = Math.floor(Math.min(cellWidth, cellHeight) * 0.6);
                ctx.font = 'bold ' + String(fontSize) + 'px Century Gothic';
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                cCenter = [cellWidth * (j + 1 / 2), cellHeight * (i + 1 / 2)];
                if (mazeArray[i][j] === maze.START) {
                    ctx.fillText('S',cCenter[0], cCenter[1]);
                } else if (mazeArray[i][j] === maze.GOAL) {
                    ctx.fillText('G',cCenter[0], cCenter[1]);
                }
                ctx.textAlign = 'start';
                ctx.textBaseline = 'alphabetic';
            }
        }
    }
}

function drawQ(x, y, canvas, cWidth, cHeight, Q, maxQ) {
    var dx = [0, cWidth, cWidth, 0];
    var dy = [0, 0, cHeight, cHeight];

    var cCenter = [cWidth * (x + 1 / 2), cHeight * (y + 1/ 2)];

    for (var dir=0; dir < 4; ++dir) {
        var c0 = [cWidth * x + dx[dir], cHeight * y + dy[dir]];
        var c1 = [cWidth * x + dx[(dir + 1) % 4], cHeight * y + dy[(dir + 1) % 4]];

        var ctx = canvas.getContext('2d');

        ctx.beginPath();
        ctx.moveTo(cCenter[0], cCenter[1]);
        ctx.lineTo(c0[0], c0[1]);
        ctx.lineTo(c1[0], c1[1]);
        ctx.closePath();

        ctx.fillStyle = gradation(Q[dir], maxQ);
        ctx.fill();
        ctx.fillStyle = 'rgb(0, 0, 0)';
    }
}

function gradation(Q, maxQ) {
    var ret = [];
    var basic;
    var dest;
    if (Q >= 0) {
        basic = [255, 255, 255];
        dest = [255, 0, 0];
    } else {
        basic = [255, 255, 255];
        dest = [0, 0, 255];
    }
    ret[0] = Math.floor((dest[0] - basic[0]) * Math.min(maxQ, Math.abs(Q)) / maxQ) + basic[0];
    ret[1] = Math.floor((dest[1] - basic[1]) * Math.min(maxQ, Math.abs(Q)) / maxQ) + basic[1];
    ret[2] = Math.floor((dest[2] - basic[2]) * Math.min(maxQ, Math.abs(Q)) / maxQ) + basic[2];

    return 'rgb(' + ret[0] + ', ' + ret[1] + ', ' + ret[2] + ')';
}

function draw(qLearning, qCanvas, agentCanvas) {
    var maze = qLearning.environment.maze;
    var environment = qLearning.environment;
    var qTable = qLearning.agent.qTable;
    var agentPos = environment.agentPos;
    var V = maze.mazeArray.length;
    var H = maze.mazeArray[0].length;
    var cellWidth = qCanvas.width / H;
    var cellHeight = qCanvas.height / V;

    //drawMaze(maze, canvas);
    drawAgent(agentPos[0], agentPos[1], agentCanvas, cellWidth, cellHeight);
    drawQTable(qTable, environment, qCanvas);
}

function drawQTable(qTable, environment, canvas) {
    var maze = environment.maze;
    var mazeArray = maze.mazeArray;
    var V = mazeArray.length;
    var H = mazeArray[0].length;
    var cellWidth = canvas.width / H;
    var cellHeight = canvas.height / V;
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    for (var i=0; i < mazeArray.length; ++i) {
        for (var j=0; j < mazeArray[0].length; ++j) {
            if (mazeArray[i][j] === maze.PASSAGE) {
                drawQ(j, i, canvas, cellWidth, cellHeight, qTable[i][j], Reward.GOAL_VALUE);
            }
        }
    }
}

function drawAgent(x, y, canvas, cellWidth, cellHeight) {
    var cCenter = [cellWidth * (x + 1 / 2), cellHeight * (y + 1/ 2)];
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.beginPath();
    ctx.fillStyle = 'rgb(0, 255, 0)';
    ctx.arc(cCenter[0], cCenter[1], Math.min(cellWidth, cellHeight) / 2, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.fillStyle = 'rgb(0, 0, 0)';
}
