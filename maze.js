var Maze = (function () {
    //const PASSAGE = 0;
    //const WALL = 1;
    const directions = [
        [-1, 0],
        [0, -1],
        [1, 0],
        [0, 1]
    ];

    var Maze = function (v, h) {
        var V = v;
        var H = h;
        if (V % 2 === 0) {
            V++;
        }
        if (H % 2 === 0) {
            H++;
        }

        this.mazeArray = [];
        for (var i=0; i < V; ++i) {
            this.mazeArray.push([]);
            for (var j=0; j < H; ++j) {
                    this.mazeArray[i].push(this.WALL);
            }
        }
    }

    var pt = Maze.prototype;

    pt.PASSAGE = 0;
    pt.WALL = 1;
    pt.START = 2;
    pt.GOAL = 3;
    pt.AGENT = 4;

    pt.generate = function () {
        var V = this.mazeArray.length;
        var H = this.mazeArray[0].length;

        var x0 = Math.floor(Math.random() * (Math.floor(H / 2) - 1)) * 2;
        var y0 = Math.floor(Math.random() * (Math.floor(V / 2) - 1)) * 2;

        this.dig(x0, y0);

        do {
            var xS = Math.floor(Math.random() * H);
            var yS = Math.floor(Math.random() * V);
            if (this.mazeArray[yS][xS] === this.PASSAGE) {
                this.mazeArray[yS][xS] = this.START;
                break;
            }
        } while(true);

        do {
            var xG = Math.floor(Math.random() * H);
            var yG = Math.floor(Math.random() * V);
            if (this.mazeArray[yG][xG] === this.PASSAGE && this.mazeArray[yG][xG] !== this.START) {
                this.mazeArray[yG][xG] = this.GOAL;
                break;
            }
        } while(true);
    }

    pt.dig = function (x, y) {
        if (this.mazeArray[y][x] !== undefined) {
            this.mazeArray[y][x] = this.PASSAGE;
        }

        var rndArray = shuffle(4);

        for (var i=0; i < rndArray.length; ++i) {
            var d = directions[rndArray[i]];
            if (this.mazeArray[y + d[1]] === undefined) {
                continue;
            }
            if (this.mazeArray[y + d[1]][x + d[0]] === undefined) {
                continue;
            }
            if (this.mazeArray[y + d[1] * 2][x + d[0] * 2] === this.WALL) {
                this.mazeArray[y + d[1]][x + d[0]] = this.PASSAGE;

                this.dig(x + d[0] * 2, y + d[1] * 2);
            }
        }
    }

    pt.debug = function () {
        for (var i=0; i < this.mazeArray.length; ++i) {
            var tmp = [];
            for (var j=0; j < this.mazeArray[0].length; ++j) {
                tmp[j] = (this.mazeArray[i][j] === this.PASSAGE) ? '□': '■';
            }
            console.log(tmp);
        }
    }

    pt.clear = function () {
        for (var i=0; i < this.mazeArray.length; ++i) {
            for (var j=0; j < this.mazeArray[0].length; ++j) {
                    this.mazeArray[i][j] = this.PASSAGE;
            }
        }
    }

    pt.set = function (x, y, t) {
        console.log('x = ' + x + ', y = ' + y + ', t = ' + t);
        if (t === this.PASSAGE || t === this.WALL) {
            this.mazeArray[y][x] = Number(!this.mazeArray[y][x]);
        } else {
            var flg = false;
            for (var i=0; i < this.mazeArray.length; ++i) {
                for (var j=0; j < this.mazeArray[0].length; ++j) {
                    if (this.mazeArray[i][j] === t) {
                        flg = true;
                        if (!(i === y && j === x)) {
                            this.mazeArray[i][j] = this.PASSAGE
                            this.mazeArray[y][x] = t;
                        }
                        i = this.mazeArray.length;
                        break;
                    }
                }
            }
            if (!flg) {
                this.mazeArray[y][x] = t;
            }
        }
    }

    return Maze;
})();

function shuffle(n) {
    var ret = [];
    for (var i=0; i < n; ++i) {
        ret[i] = i;
    }

    var rnd;
    var tmp;

    for (var i=n-1; i > 0; --i) {
        rnd = Math.floor(Math.random() * (i+1));
        tmp = ret[i];
        ret[i] = ret[rnd];
        ret[rnd] = tmp;
    }

    return ret;
}
//
//
//window.onload = function () {
//    var canvas = document.getElementById('mainCanvas');
//    var SIZE = 20;
//    var V = 31;
//    var H = 31;
//    canvas.width = H * SIZE;
//    canvas.height = V * SIZE;
//
//    var m = new Maze(V, H);
//    m.generate();
//
//    drawMaze(m, canvas);
//}
