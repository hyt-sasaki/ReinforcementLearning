/* 学習モデル */
var Model = (function() {
    /* コンストラクタ */
    var Model = function(c) {
        this.centroids = c; //初期セントロイド集合
        this.clusterIndices = new Array;
        for (var i=0; i < this.centroids.length; ++i) {
            this.clusterIndices[i] = new Array;
        }
    }

    var pt = Model.prototype;

    pt.judge = function (x) {
        var minDist = Number.MAX_VALUE;
        var minDistIdx = -1;
        for (var i=0; i < this.centroids.length; ++i) {
            var dist = l2distnce(x, this.centroids[i]);
            if (dist < minDist) {
                minDist = dist;
                minDistIdx = i;
            }
        }

        return minDistIdx;
    }

    return Model;
})();

function l2distnce(x, y) {
    var ret = 0;
    for (var i=0; i < x.length; ++i) {
        ret += Math.pow(x[i] - y[i], 2);
    }
    ret = Math.sqrt(ret);

    return ret;
}
