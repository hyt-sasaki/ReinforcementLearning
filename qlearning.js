var QLearning = (function () {
    var SPLIT = 10;

    var QLearning = function (agent, environment, episodeLimit, stepLimit) {
        this.agent = agent;
        this.environment = environment;
        this.episodeLimit = episodeLimit;
        this.stepLimit = stepLimit;

        this.epsilonList = [];
        for (var i=0; i <= SPLIT; ++i) {
            this.epsilonList.push((SPLIT - i) / SPLIT);
        }

        this.episode = 0;
        this.step = 0;
        this.minStep = this.stepLimit + 1;
    }

    var pt = QLearning.prototype;

    pt.oneStep = function () {
        if (this.step === 0) {
            this.environment.resetAgentPos();
            this.schedule();
            var state0 = this.environment.getAgentState();
            this.agent.observe(state0);
        }
        var action = this.agent.getNextAction();
        var reward = this.environment.getAgentReward(action);
        this.environment.transNextEnv(action);
        var state = this.environment.getAgentState();
        this.agent.calcQTable(action, reward, state);
        this.agent.observe(state);
        this.step++;
        if (reward === Reward.GOAL_VALUE || this.step === this.stepLimit) {
            if (reward === Reward.GOAL_VALUE) {
                if (this.step < this.minStep) {
                    this.minStep = this.step;
                }
            }
            this.episode++;
            this.step = 0;
        }
    }

    pt.schedule = function () {
        var episodeSplit = Math.floor(this.episodeLimit / (SPLIT + 1));
        if (this.episode % episodeSplit === 0) {
            this.agent.epsilon = this.epsilonList[Math.floor(this.episode / episodeSplit)];
        }
    }

    return QLearning;
})();

var Agent = (function () {
    var Agent = function (a, g, V, H) {
        this.alpha = a;
        this.gamma = g;
        this.epsilon = 1.0;
        this.initQTable(V, H);
    }

    var pt = Agent.prototype;

    pt.observe = function (state) {
        this.observation = state;
    }

    pt.initQTable = function (V, H) {
        this.qTable = [];
        for (var i=0; i < V; ++i) {
            this.qTable[i] = [];
            for (var j=0; j < H; ++j) {
                this.qTable[i][j] = [];
                for (var k=0; k < 4; ++k) {
                    this.qTable[i][j][k] = 0;
                }
            }
        }
    }

    pt.calcQTable = function (action, reward, state) {
        var x = this.observation.x;
        var y = this.observation.y;
        var q1 = (1 - this.alpha) * this.qTable[y][x][action];
        var maxQ = this.getMaxQ(state);
        var q2 = this.alpha * (reward + this.gamma * maxQ);
        this.qTable[y][x][action] = q1 + q2;
    }

    pt.getNextAction = function () {
        var rnd = Math.random();

        if (rnd < this.epsilon) {
            return Math.floor(Math.random() * Object.keys(Action).length);
        } else {
            return this.getMaxQAction(this.observation);
        }
    }

    pt.getMaxQ = function (state) {
        var x = state.x;
        var y = state.y;
        var maxQ = this.qTable[y][x][0];
        for (var i=1; i < Object.keys(Action).length; ++i) {
            var q = this.qTable[y][x][i];
            if (q > maxQ) {
                maxQ = q;
            }
        }

        return maxQ;
    }

    pt.getMaxQAction = function (state) {
        var x = state.x;
        var y = state.y;
        var maxQ = this.qTable[y][x][0];
        var action = 0;
        for (var i=1; i < Object.keys(Action).length; ++i) {
            var q = this.qTable[y][x][i];
            if (q > maxQ) {
                maxQ = q;
                action = i;
            }
        }

        return action;
    }

    return Agent;
})();

var State = (function () {
    var State = function (pos) {
        this.x = pos[0];
        this.y = pos[1];
    }

    return State;
})();

var Action = {
    UP: 0,
    RIGHT: 1,
    DOWN: 2,
    LEFT: 3
}

var ActionToDirection = [
    [0, -1],
    [1, 0],
    [0, 1],
    [-1, 0]
];

var Reward = {
    GOAL_VALUE: 100,
    OBS_VALUE: -5,
    FREE_VALUE: -1
}

var Environment = (function () {
    var Environment = function (maze) {
        this.maze = maze;
    }

    var pt = Environment.prototype;

    pt.resetAgentPos = function () {
        var mazeArray = this.maze.mazeArray;
        for (var y=0; y < mazeArray.length; ++y) {
            for (var x=0; x < mazeArray[0].length; ++x) {
                if (mazeArray[y][x] === this.maze.START) {
                    this.agentPos = [x, y];
                    return;
                }
            }
        }

    }

    pt.isMovable = function (pos) {
        var x = pos[0];
        var y = pos[1];
        if (this.maze.mazeArray[y] === undefined) {
            return false;
        }
        if (this.maze.mazeArray[y][x] === undefined) {
            return false;
        }
        if (this.maze.mazeArray[y][x] === this.maze.WALL) {
            return false;
        }

        return true;
    }

    pt.getNextPos = function (action) {
        var x = this.agentPos[0];
        var y = this.agentPos[1];

        var d = ActionToDirection[action];

        return [x + d[0], y + d[1]];
    }

    pt.transNextEnv = function (action) {
        var nextPos = this.getNextPos(action);

        if (this.isMovable(nextPos)) {
            this.agentPos = nextPos;
        }
    }

    pt.getAgentState = function () {
        return new State(this.agentPos);
    }

    pt.getAgentReward = function (action) {
        var nextPos = this.getNextPos(action);

        if (!this.isMovable(nextPos)) {
            return Reward.OBS_VALUE;
        } else {
            if (this.maze.mazeArray[nextPos[1]][nextPos[0]] === this.maze.GOAL) {
                return Reward.GOAL_VALUE;
            } else {
                return Reward.FREE_VALUE;
            }
        }
    }

    return Environment;
})();
