var KMeans = (function () {
    /* コンストラクタ */
    var KMeans = function (m, i) {
        this.model = m; //学習モデル
        this.itr = i;   //学習回数
    }

    var pt = KMeans.prototype;

    /* 学習 */
    pt.train = function (X) {
        var itrCount = 0;
        do {
            var oldClusterIndices = this.model.centroids.slice();
            this.update(X);
            itrCount++;
            if (itrCount >= this.itr) {
                break;
            }
            if (this.check(oldClusterIndices)) {
                if (itrCount !== 1) {
                    break;
                }
            }
        } while(true);
    }

    pt.update = function (X) {
        this.calcCentroids(X);
        this.assignCluster(X);
    }

    pt.check = function (oldClusterIndices) {
        for (var i=0; i < oldClusterIndices.length; ++i) {
            var flg = false;
            for (var j=0; j < this.model.centroids.length; ++j) {
                if (oldClusterIndices[i] === this.model.centroids[j]) {
                    flg = true;
                    break;
                }
            }
            if (!flg) {
                return false;
            }
        }
        return true;
    }

    pt.assignCluster = function (X) {
        for (var i=0; i < this.model.clusterIndices.length; ++i) {
            this.model.clusterIndices[i] = [];
        }
        for (var i=0; i < X.length; ++i) {
            var cluster = this.model.judge(X[i]);
            this.model.clusterIndices[cluster].push(i);
        }
    }

    pt.calcCentroid = function (X, n) {
        if (this.model.clusterIndices[n].length === 0) {
            return this.model.centroids[n];
        }
        var x = X[this.model.clusterIndices[n][0]];
        if (this.model.clusterIndices[n].length === 1) {
            return x;
        }
        for (var i=1; i < this.model.clusterIndices[n].length; ++i) {
            x = vectorAddition(x, X[this.model.clusterIndices[n][i]]);
        }

        x = vectorDevision(x, this.model.clusterIndices[n].length);

        return x;
    }

    pt.calcCentroids = function (X) {
        for (var i=0; i < this.model.centroids.length; ++i) {
            this.model.centroids[i] = this.calcCentroid(X, i);
        }
    }

    return KMeans;
})();

function vectorAddition(x, y) {
    var ret = new Array;
    for (var i=0; i < x.length; ++i) {
        ret[i] = x[i] + y[i];
    }

    return ret;
}

function vectorDevision(x, n) {
    var ret = new Array;
    for (var i=0; i < x.length; ++i) {
        ret[i] = x[i] / n;
    }

    return ret;
}
