$(function () {
    /* 訓練データの設定 */
    var mazeCanvas = $('#mazeCanvas')[0];
    var qCanvas = $('#qCanvas')[0];
    var agentCanvas = $('#agentCanvas')[0];
    var clickedCanvas = $('#clickedCanvas')[0];

    var mRef = {maze:null};
    var tRef = {timer:null, isWork:false};
    var qRef = {qLearning:null};

    /* イベントハンドラの設定 */
    $('#clickedCanvas').on('click', {mRef:mRef, mazeCanvas:mazeCanvas}, onClicked);
    $('#generate').on('click', {mRef:mRef, mazeCanvas:mazeCanvas}, onGenerate);
    $('#learn').on('click', {qRef:qRef, tRef:tRef, mRef:mRef, qCanvas:qCanvas, agentCanvas:agentCanvas}, onLearn);
    $('#stop').on('click', {tRef:tRef}, onStop);
    $('#play').on('click', {qRef:qRef, tRef:tRef, qCanvas:qCanvas, agentCanvas:agentCanvas}, onReplay);
    $('#allResetButton').on('click', {mRef:mRef, mazeCanvas:mazeCanvas, qCanvas:qCanvas, agentCanvas:agentCanvas, tRef:tRef}, onReset);
    $(window).on('keydown', onShiftkeyDown);
    $(window).on('keydown', onEnterkeyDown);
    $(window).on('keydown', {tRef:tRef}, onSpacekeyDown);
});

function onShiftkeyDown(event) {
    if (event.shiftKey) {
        var input = $('input[name="mazeCell"]');
        if (input.eq(0).prop('checked')) {
            input.eq(1).prop('checked', true);
        } else if (input.eq(1).prop('checked')) {
            input.eq(2).prop('checked', true);
        } else if (input.eq(2).prop('checked')) {
            input.eq(0).prop('checked', true);
        }
    }
}

function onEnterkeyDown(event) {
    if (event.which === 13) {
        $('#learn').trigger('click');
    }
}

function onSpacekeyDown(event) {
    if (event.which === 32) {
        if (event.data.tRef.isWork) {
            $('#stop').trigger('click');
        } else {
            $('#play').trigger('click');
        }
    }
}

function onReset(event) {
    var V = $('#V').val();
    var H = $('#H').val();
    event.data.mRef.maze = new Maze(V, H);
    event.data.mRef.maze.clear();
    clearInterval(event.data.tRef.timer);
    event.data.tRef.isWork = false;
    var out = $('#output span');
    out.children().remove();

    var mazeCanvas = event.data.mazeCanvas;
    var qCanvas = event.data.qCanvas;
    var agentCanvas = event.data.agentCanvas;
    mazeCanvas.getContext('2d').clearRect(0, 0, mazeCanvas.width, mazeCanvas.height);
    qCanvas.getContext('2d').clearRect(0, 0, qCanvas.width, qCanvas.height);
    agentCanvas.getContext('2d').clearRect(0, 0, agentCanvas.width, agentCanvas.height);
}

function onStop(event) {
    clearInterval(event.data.tRef.timer);
    event.data.tRef.isWork = false;
}

function onReplay(event) {
    var out = $('#output');
    var span = out.children('span');
    var qLearning = event.data.qRef.qLearning;
    var timer = event.data.tRef.timer;
    var qcanvas = event.data.qCanvas;
    var agentCanvas = event.data.agentCanvas;
    if (!event.data.tRef.isWork && qLearning.environment.maze !== undefined) {
        event.data.tRef.isWork = true;
        timer = setInterval(function () {
            try {
                qLearning.oneStep();
            } catch(e) {
                clearInterval(timer);
                event.data.tRef.isWork = false;
                return;
            }
            draw(qLearning, qCanvas, agentCanvas);
            var episodeLimit = qLearning.episodeLimit;
            var minStep = (qLearning.minStep > episodeLimit) ? '-':qLearning.minStep;
            span.html('<br />エピソード数 = ' + qLearning.episode +
                      '<br />ステップ数 = ' + qLearning.step +
                      '<br />最小ステップ数 = ' + minStep +
                      '<br />ε= ' +qLearning.agent.epsilon);
            if (qLearning.episode === qLearning.episodeLimit) {
                clearInterval(timer);
                event.data.tRef.isWork = false;
            }
        }, 10);
    }
    event.data.tRef.timer = timer;
}

function onLearn(event) {
    alert('学習開始');
    var env = new Environment(event.data.mRef.maze);
    var alpha = Number($('#alpha').val());
    var gamma = Number($('#gamma').val());
    var V = Number($('#V').val());
    var H = Number($('#H').val());
    var agent = new Agent(alpha, gamma, V, H);
    var episodeLimit = Number($('#episodeLimit').val());
    var stepLimit = Number($('#stepLimit').val());
    var R_wall = Number($('#R_wall').val());
    var R_step = Number($('#R_step').val());
    var R_goal = Number($('#R_goal').val());
    Reward.GOAL_VALUE = R_goal;
    Reward.OBS_VALUE = R_wall;
    Reward.FREE_VALUE = R_step;
    event.data.qRef.qLearning = new QLearning(agent, env, episodeLimit, stepLimit);
    var qLearning = event.data.qRef.qLearning;
    clearInterval(event.data.tRef.timer);
    event.data.tRef.isWork = false;
    var out = $('#output > span');
    out.children().remove();
    var span = $('<span>');
    out.append(span);
    var qCanvas = event.data.qCanvas;
    var agentCanvas = event.data.agentCanvas;
    if (qLearning.environment.maze !== undefined) {
        event.data.tRef.isWork = true;
        event.data.tRef.timer = setInterval(function () {
            try {
                qLearning.oneStep();
            } catch(e) {
                clearInterval(event.data.tRef.timer);
                event.data.tRef.isWork = false;
                return;
            }
            draw(qLearning, qCanvas, agentCanvas);
            var minStep = (qLearning.minStep > episodeLimit) ? '-':qLearning.minStep;
            span.html('<br />エピソード数 = ' + qLearning.episode +
                      '<br />ステップ数 = ' + qLearning.step +
                      '<br />最小ステップ数 = ' + minStep +
                      '<br />ε= ' + qLearning.agent.epsilon);
            if (qLearning.episode === qLearning.episodeLimit) {
                clearInterval(event.data.tRef.timer);
                event.data.tRef.isWork = false;
            }
        }, 10);
    }
}

function onGenerate(event) {
    var V = Number($('#V').val());
    var H = Number($('#H').val());
    event.data.mRef.maze = new Maze(V, H);
    event.data.mRef.maze.generate();
    var mazeCanvas = event.data.mazeCanvas;
    drawMaze(event.data.mRef.maze, mazeCanvas);
}

function onClicked(event) {
    var canvasRect = event.target.getBoundingClientRect();
    var cx = event.clientX - canvasRect.left;
    var cy = event.clientY - canvasRect.top;
    var V = Number($('#V').val());
    var H = Number($('#H').val());
    if (event.data.mRef.maze === null) {
        event.data.mRef.maze = new Maze(V, H);
        event.data.mRef.maze.clear();
    }

    var x = Math.floor(cx / clickedCanvas.width * H);
    var y = Math.floor(cy / clickedCanvas.height * V);

    var t;
    var mazeCell = $('input[name="mazeCell"]:checked').val();
    if (mazeCell == "pw") {
        t = event.data.mRef.maze.PASSAGE;
    } else if (mazeCell == "s") {
        t = event.data.mRef.maze.START;
    } else if (mazeCell == "g") {
        t = event.data.mRef.maze.GOAL;
    }
    event.data.mRef.maze.set(x, y, t);
    drawMaze(event.data.mRef.maze, event.data.mazeCanvas);
}
